(function($){
    function resizePageHeader() {
        var widthPHFilterDefaultHeight = parseInt( $('.page-header__filter__mrt-center').css('min-height') );
        var widthPHFilterDefaultPadding = ($(window).width() < 640) ? 260 : 0;
        
        var widthPHFilterHeight = $('.page-header__filter__mrt-center').height(); //y
        var widthPHInnerHeight = $('.page-header__inner__mrt-center').height();   //x
        
        
        if (widthPHFilterHeight > widthPHFilterDefaultHeight) {
            var differ = widthPHFilterHeight - widthPHFilterDefaultHeight;
            $('.page-header__inner__mrt-center').css({'padding-bottom' : differ + widthPHFilterDefaultPadding});
        }
    }
    setTimeout(resizePageHeader, 150);
    
    $('.mrt-map__sidebar__catalog').on('click', '.mrt-map__link', function () {
        $(this).parents('.mrt-map__sidebar__catalog').toggleClass('mrt-map__sidebar__catalog-link');
    });
    
    
    $('.catalog__sort-arrow').on('click', function () {
        if ($(this).hasClass('sort_desc')) {
            $(this).addClass('sort_asc').removeClass('sort_desc');
        }
        else {
            $(this).addClass('sort_desc').removeClass('sort_asc');
        }
        $(this).siblings().removeClass('sort_asc sort_desc');
    });
    
    var hiddenBlock = $('.content__hide-block');
    if (hiddenBlock.length) {
        var hiddenBlockHtml = hiddenBlock.html().split('<hr>');
        if (hiddenBlockHtml.length > 1) {
            var hiddenBlockHtmlNew = '<div class="content__inner">' +
                                        '<div class="content__visible">' +
                                            hiddenBlockHtml[0] +
                                        '</div>' +
                                        '<div class="content__hidden">' +
                                            hiddenBlockHtml[1] +
                                        '</div>' +
                                    '</div>' +
                                    '<a href="javascript:" class="more-link">Читать далее</a>';
                                    
            hiddenBlock.html(hiddenBlockHtmlNew);
        }
        
        hiddenBlock.on('click', '.more-link', showHideContent);
    }
    
    $(window).on('scroll.callto', function () {
        var scrolled = window.pageYOffset;
        if (scrolled > 225) {
            $('.call-now').fadeIn();
            
            $(window).off('scroll.callto');
        }
        else {
            $('.call-now').fadeOut();
        }
    });
    
    function showHideContent () {       
        var textShow = 'Читать далее';
        var textHide = 'Скрыть';
        
        $(this).parents('.content__hide-block').find('.content__hidden').fadeToggle('fast'); 
        $(this).toggleClass('show');
        if ($(this).html() == textShow) { 
            $(this).html(textHide) 
        } 
        else { 
            $(this).html(textShow) 
        } 
        return false;
    }
    
    var CenterListSwitcher = {
        placemarks: {},
        clickedMarkerId: null,
        myMap: null,
    
        drawMap: function(result) {
                if ($('#map').length == 0) {
                  return;
                }
                var map_zoom = 11;
                var center = {'lng': 30.258829621598125, 'lat': 59.90703163833079};
                if (!this.myMap) {
                  this.myMap = new ymaps.Map('map', {
                    center: [center['lat'], center['lng']],
                    zoom: map_zoom
                  }, {
                    searchControlProvider: 'yandex#search'
                  });
                } else {
                   for (key in this.placemarks) {
                     this.myMap.geoObjects.remove(this.placemarks[key]); 
                   }
                }
                this.placemarks = {};
                this.clickedMarkerId = null;
                for (key in result['locationResult']) {
                  myPlacemark = new ymaps.Placemark([result['locationResult'][key]['latitude'],
                    result['locationResult'][key]['longitude']], {
                      balloonContent: '<div class="balloon">\
                            <div class="rating" style="width: auto">\
                                <div class="rating__state">\
                                    <div class="rating__state-val" style="width: '+result['locationResult'][key]['rating'] * 20+'%"></div>\
                                </div>\
                            </div>\
                            <a class="balloon__title" href="'+result['locationResult'][key]['link']+'">'+result['locationResult'][key]['name']+'</a>\
                            <div class="balloon__price">\
                              '+result['locationResult'][key]['firstPriceName']+' - \
                              '+result['locationResult'][key]['firstPriceVal']+' руб. \
                            </div>\
                            <div class="balloon__phone-title">Телефон для записи</div>\
                            <a href="tel:'+result['phone']+'" class="balloon__phone phone-number">'+result['phone']+'</a>\
                          </div>',
                      id: result['locationResult'][key]['id']
                  }, {
                    balloonLayout: "default#imageWithContent",
                    balloonImageHref: '',
                    openBalloonOnClick: false,
                    hideIconOnBalloonOpen: false,
                    balloonAutoPan: false,
                    balloonPanelMaxMapArea: 0,
                    balloonOffset: [-125, -50],
                    iconLayout: 'default#image',
                    iconImageHref: '/img/marker_catalog.png',
                    iconImageSize: [43, 61],
                    iconImageOffset: [-22, -61],
                    pane: 'balloon',
                    zIndex: 100
                  }); 
                  myPlacemark.events.add('click', function (e) {
                    e.get('target').balloon.close(); 
                    e.get('target').options.set('iconImageHref', "/img/marker_catalog.png");
                    var object = e.get('target');
                    var id = object.properties.get('id');  
                    CenterListSwitcher.clickMarker(id);                
                  });
                  myPlacemark.events
                  .add('mouseenter', function (e) {
                    var pos = e.get('target').balloon.getPosition();
                    //console.log(pos);
                    e.get('target').balloon.open(e.get('target').balloon.getPosition(), true); 
                    e.get('target').options.set('iconImageHref', "/img/marker_catalog__hover.png");
                  })
                  .add('mouseleave', function (e) {
                    //e.get('target').balloon.close(); 
                  });
                  this.myMap.geoObjects.add(myPlacemark); 
                  CenterListSwitcher.placemarks[result['locationResult'][key]['id']] = myPlacemark; 
                }
                this.myMap.behaviors.disable('scrollZoom');      
    },
    
    loadMap: function(result) {
              if (typeof ymaps == 'undefined') {
                return;
              }
              ymaps.ready(function() {
                CenterListSwitcher.drawMap(result);
              });      
    },
    
    
    clickMarker: function(id) {
      if ($('body').width() > 1200) {
        var elementToSroll = $('#card-item-' + id);
        elementToSroll.get(0).scrollIntoView();
        if (elementToSroll.parents('#centers-block-hidden').length) {
          $('#centers-block-hidden').show();
          $('#center-list-show-more').hide();
        }
        var scrolledY = window.scrollY;
        if (scrolledY){
          window.scroll(0, scrolledY - 30);
        }
        setTimeout(function() {
          if (elementToSroll.index() == 0) {
            ScrollMapBlock.setTopPos();
          } else if (elementToSroll.index() == ($('.card-item').length - 1)) {
            ScrollMapBlock.setBottomPos();
          }          
        }, 100);
      }
      //redraw placemarks
      if (this.clickedMarkerId != null) {
        CenterListSwitcher.placemarks[this.clickedMarkerId].options.set({iconImageHref: '/img/marker_catalog.png', 'iconImageSize': [43, 61], iconImageOffset: [-22, -61]});
      }
      CenterListSwitcher.placemarks[id].options.set({iconImageHref: '/img/marker_catalog_active.png', 'iconImageSize': [43, 61], iconImageOffset: [-22, -61]});
      this.clickedMarkerId = id;

    },
    
    init: function() {
      this.loadMap(window.locationResult);
    }
  }
  
  CenterListSwitcher.init();
    
})($);