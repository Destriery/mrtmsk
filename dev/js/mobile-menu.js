(function($){
    $(document).ready(function () {
        mobileMenu();
        modals();
    });
    
    function mobileMenu() {
        
        var mobileMenu = $('.mobile-menu');
        var menuWidth = getComputedStyle(mobileMenu[0]).width;
        var procOrPx = (menuWidth.indexOf('%') + 1) ? '%' : 'px';

        $('.close__icon').on('click', function () {
            mobileMenu.animate({left: '-'+menuWidth}, function () {
                    $(this).css({'display': 'none'});
            });
        });
        
        $('.nav-link').on('click', function () {
            mobileMenu.css({'display': 'block'});
            mobileMenu.animate({left: '0'+procOrPx });
        });
        
    }
    
    function modals() {
        var body = $('body');
        var overlay = $('.overlay');
        var buttonsCallback = $('.button-callback');
        var modals = $('.modal');
        
        // show/hide
        buttonsCallback.on('click', function () {
            showModal('callback');
        });
        
        $('.modal__close').on('click', hideModal);
        overlay.on('click', function (event) {
            if (event.target == overlay[0]) {
                hideModal();
            }
        });
        
        //valid
        modals.find('form').on('submit', checkValid);
        modals.find('input, textarea').on('focus', function () {
            $(this).removeClass('input__error')
                .parents('.modal').find('.modal__error').removeClass('show');
        });
        
        
        
        function showModal(id) {
            $('#'+id).css({display: 'block'});
            overlay.fadeIn();
            //body.css({overflow: 'hidden'});
        }
        
        function hideModal() {
            overlay.fadeOut();
            modals.fadeOut();
            //body.css({overflow: 'auto'});
        } 
        
        function checkValid (event) {
            var $this = $(this);
            var requiredFields = ['callback_phone'];
            var error = false;
            
            requiredFields.forEach(function (item, i) {
                var elem = $this.find('[name="'+item+'"]');
                if (!!elem.length) {
                    if ( !elem.val() ) {
                        elem.addClass('input__error');
                        $this.find('.modal__error').addClass('show');
                        error = true;
                    }
                }
            });
            
            if (error) {
                event.preventDefault(); 
                (event.cancelBubble) ? event.cancelBubble : event.stopPropagation;
            }
            
        }
    }
    
})($);