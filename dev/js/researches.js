(function($){
    $('.price__item.show_all').on('click', showHidePrices);
    $('.tabs__title').on('click', showHideResearchesTabs);
    
    function showHidePrices () {
        var isCatalogPrice = !!$(this).parents('.price__list__catalog').length;        
        var textShow = isCatalogPrice ? '<span>Еще услуги</span>' : 'Показать все';
        var textHide = isCatalogPrice ? '<span>Скрыть</span>' : 'Скрыть';
        
        $(this).parents('.price__items').find('.hidden').fadeToggle('fast'); 
        $(this).toggleClass('show');
        if ($(this).html() == textShow) { 
            $(this).html(textHide) 
        } 
        else { 
            $(this).html(textShow) 
        } 
        return false;
    }
    
    function showHideResearchesTabs () {
        var index = $(this).index();
        
        $(this).addClass('active').siblings().removeClass('active');
        $('.tabs__content-item').eq(index).addClass('active').siblings().removeClass('active');
        
        if ($(this).text() == "Врачи") {
            var col = Math.round($('.aligning').width()/$('.aligning').find('.col-item')[0].clientWidth);
            Aligning('.aligning', col);
        }
    }
    
    
    $('.mrt-center__rating__set').on('mousemove', '.rating__state', function (event) {
        
        var width = $(this).width(),
            offset = $(this).offset();
         
        var left = event.pageX - offset.left;
        var proc = Math.ceil((left*100/width)/20)*20;
        $(this).find('.rating__state-val').css({width: proc+'%'});
        
    }).on('mouseleave', '.rating__state', function () {
        
        var result = !!$(this).data('width') ? $(this).data('width') : '0%';
        
        $(this).find('.rating__state-val').css({width: result});
        
    }).on('click', '.rating__state', function () {
        var width = $(this).find('.rating__state-val').width();
        var stars = Math.round(width*10/$(this).width())/2;
        
        $(this).data('width', width);
        $(this).parents('form').find('[name="rating_bottom"]').val(stars);
        
    });
    
    function Aligning (container, col) {
        var temp = [0,0];
        var items = $(container).find('.col-item');
        items.removeAttr('style');
        
        items.each(function (i, item) {
            if (!(i % col)) {
                temp[0] = items[i].clientHeight;
                temp[1] = items[i].clientHeight;
            }
            if (items[i].clientHeight > temp[1]) {
                temp[1] = items[i].clientHeight;
            } 
            else {
                temp[0] = items[i].clientHeight;    
            }
            if (temp[0] < temp[1] && (i % col == col-1 || i+1 == items.length)) {
                var curCol = i % col;
                setHeight(temp[1], i, curCol);
            }
        });
        
        function setHeight (height, i, curCol) {
            
            for (var j = i; j >= i-curCol; j--) {
                
                items.eq(j).css({height: height})
                
            }
        }
    }
    
})($);