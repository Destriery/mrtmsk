
import './select';
import './researches';
import './mobile-menu';
import './catalog';
import './libs/slick/slick';
import './libs/jquery.accordion';

//** polyfill for <use xlink:href="symbol-defs.svg#icon-edit">
//import 'svgxuse';

$('.js-select').customSelect({
    list: 'custom-select__list',
    pseudo: 'custom-select__pseudo',
    dropdown: 'custom-select__dropdown',
    link: 'custom-select__link'
});

$('.comments__wrapper').slick({
    arrows: false,
    dots: true,
    fade: false,
    cssEase: 'linear',
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    rows: 2,
    appendDots: $('.slick-dots__wrapper')
});

$('.slick-slider__main').slick({
    arrows: false,
    dots: false,
    fade: true,
    cssEase: 'linear',
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    //asNavFor: '.slick-slider__preview'
});

$('.slick-slider__preview').on('click', '.item', function () {
    var index = $(this).index();
    
    $(this).addClass('slick-current').siblings().removeClass('slick-current');
    $('.slick-slider__main').slick('goTo', index);
});

$('.tprice').accordIon({
    tabs_class : '.tprice__header',
    blocks_class : '.tprice__body',
    indicators_class : '.tprice__header-arrow'
});