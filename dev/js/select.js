    (function($){
        $body = $('body');
        
        $.fn.customSelect = function(options){
            this.each(function(i, el){
                this.select = $(this);

                var $list, $pseudo, $dropdown, $link, $options, $searchInput;

                this._buildList = function(){
                    $options = this.select.find('optgroup, option');

                    $dropdown = $('<ul>').addClass(options.dropdown);
                    $link = $('<li>').addClass(options.link);
        
                    //$dropdown.append($link.clone().html('<input type="text">').addClass('search-input'));

                    $options.each(function(i, el){
                        if ($(el).is('optgroup')) {
                            $dropdown.append($link.clone().text($(el).attr('label')).addClass('custom-select_optgroup'));
                        } else {
                            if (!i && !!$(el).data('placeholder')) {
                                $dropdown.append($link.clone().text($(el).data('placeholder')).data('text',$(el).text()));
                            }
                            else {
                                $dropdown.append($link.clone().text($(el).text()));
                            }
                        }
                    });
                };

                this._buildSelect = function(){
                    this._buildList();

                    $list = $('<div>').addClass(options.list);
                    $pseudo = $('<div>').addClass(options.pseudo);
                    $searchInput = $('<input>').addClass('custom-select_search custom-select__pseudo');

                    this.select.wrap($list);
                    this.select.parent().prepend($dropdown).prepend($pseudo).prepend($searchInput);

                    this.list     = this.select.parent($list);
                    this.pseudo   = this.list.find($pseudo);
                    this.dropdown = this.list.find($dropdown);
                    this.links    =	this.dropdown.children();
                    this.searchInput = this.list.find($searchInput);
                };
                this._defaultVal = function(){
                    this._setVal();
                };
                this._defaultCSS = function(){
                    this.select.css('display', 'none');
                };
                this._selection = function(event){
                    var $curLink = $(event.currentTarget); // current link
        
                    if ($curLink.hasClass('search-input') || $curLink.hasClass('custom-select_optgroup')) {
                        return;
                    }
      
                    if ($curLink.hasClass('is-active')){
                        this._hideList();
                        return;
                    };

                    $options.removeProp('selected');
                    $options.removeAttr('selected');
                    var opt = $options.parent().find('option').filter(function() {
                        var curLinkText = !!$curLink.data('text') ? $curLink.data('text') : $curLink.text();
                        return $(this).text() === curLinkText;
                    });
                    opt.prop('selected', true);
                    opt.attr('selected', true);

                    this.select.trigger('change');
                }.bind(this);
                this._setVal = function(){
                    var curOption = this.select.find('option').filter(':selected');
        

                    this.links.removeClass('is-active');
                    this.links.find('.custom-ok-pipka').remove();
                    this.pseudo.text(curOption.text());
                    var link = this.links.filter(function() {
                        var curOptionText = !!curOption.data('placeholder') ? curOption.data('placeholder') : curOption.text();
                        return $(this).text() === curOptionText;
                    });
                    link.addClass('is-active').append('<span class="custom-ok-pipka"></span>');

                    this._hideList();
                };
                this._narrowResult = function(e) {
                    var val = $(e.currentTarget).parent().find('.custom-select_search').val();
                    $(e.currentTarget).closest('div').find('li:gt(0)').each(function() {
                        if ($(this).hasClass('custom-select_optgroup')) {
                            return;
                        }
                        if ($(this).text().toLowerCase().indexOf(val.toLowerCase()) == -1) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    })
                };
                this._showList = function(){
                    this.list.toggleClass('is-open');
                    this.list.closest('.custom-select').find('.custom-select__icon').toggleClass('is-open');
                    this.dropdown.slideToggle(300);
                    this.pseudo.hide();
                    this.searchInput.show();
                }.bind(this);
                this._hideList = function(){
                    $('.custom-select__icon').removeClass('is-open');
                    this.list.removeClass('is-open');
                    this.dropdown.slideUp(300);
                    this.searchInput.hide();
                    this.pseudo.show();
                };
                this.init = function(){
                    this._buildSelect();
                    this._defaultVal();
                    this._defaultCSS();

                    this.pseudo.on('click', this._showList);
                    this.links.on('click', this._selection);
                    this.select.on('change', this._setVal);
                    this.searchInput.on('keyup', this._narrowResult);
                    $body.on('click', function(event){
                        var $target = $(event.target);
                        if($target.parents().hasClass(options.list)) return;
                        this._hideList();
                    }.bind(this));
                };
                this.init();
            });
        };
    })($);